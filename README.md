﻿# Twitter data analysis based on geo-tagged locations

Both twitter_data.sh and twitter_analysis.py are files that analyse twitter data to find tweets based on the specific word "hedde" and give the geo-tagged location of the tweet.
With this we would we able to determine where this word is used more frequently.
Twitter_data.sh gets the data from the twitter data corupus.
twitter_analysis.py analyses the data, so we can get the count of the locations.

## Installation

The only requirement is that your computer should have python 3 installed on it.

## Usage

```python
twitter_data.sh:

To be able to use this bashscript you will have to go
to the twitter corpus of the Univeristy of Groningen, 
From here you need to go to the month February, this can be done with
the following code: "cd /net/corpora/twitter2/Tweets/2019/02"

When you are at this directory in the terminal,
you should run the code in the bashscript:

zless *.out.gz | ruby -F'"text":' -ane  'puts $F' 
| grep -w -i 'hedde' | ruby -F'"location":' -ane  'puts $F' 
| awk -F',"' '{print $1}'

This will give as output all tweets which the meta data of the user
contains the word "hedde"or "Hedde". Beneath every tweet will be the geo-tagged
location.


To get all the output can take a while approx. 30min
But when the output is finished you will have to manually copy paste
the output in a text file called "twitter_data.txt"


twitter_analysis.py:

The "twitter_data.txt" text file will be input for 
the python script "twitter_analysis.py"
To run this script you need to enter the following command:

python3 twitter_analysis.py

Make sure the twitter_data.txt is in the same directory as this script.

As output the script gives the locations with the count of how many
times they appear, and also gives the total of valid locations,
and the total of all locations (including "null" as location).



```

## Contributing
Pull requests are welcome. For changes, please open an issue first to discuss what you would like to change.
