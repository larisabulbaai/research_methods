#!/usr/bin/python3
# File name: twitter_analysis.py
# This programs analyses data gotten from a txtfile.
# Author: Larisa Bulbaai
# Date: 29-03-2019

import sys
import itertools
from collections import Counter
import operator


def main():

    valid_locations = []

    # Will open the twitter_data.txt, read the file per line.
    with open('twitter_data.txt', 'r') as f:
        sentences = f.readlines()

    # Turn every line in lowercase and put in a list.
    list_data = [x.strip().lower() for x in sentences]

    # Turn list into dictionary, by getting the lines from the list,
    # and put them in the dictionary by pair.
    dictionary = dict(itertools.zip_longest(*[iter(list_data)]
                                            * 2, fillvalue=""))

    # Removes all the dictionary elements that do not contain the word
    # "hedde" in the key(the tweet).
    for key in list(dictionary.keys()):
        if "hedde" not in key:
            del dictionary[key]

    incl_null = len(dictionary)

    # Puts only the dictionary elements that do not contain the word
    # "null" as value(the location) in a list called valid_locations.
    for value in dictionary.values():
        if "null" not in value:
            valid_locations.append(value)

    total_locations = len(valid_locations)
    location_count = (Counter(valid_locations))

    # Gives the count of how many times the same value appears
    # in the dictionary, here the value is the location. 
    for k, v in location_count.most_common():
        print("Location:{0:50}  Count:{1}".format(k, v))

    print("\n""Total of locations:{0:45}".format(total_locations))
    print("Total of locations including post with null as location:{0:8}"
          "\n".format(incl_null))


if __name__ == "__main__":
    main()
