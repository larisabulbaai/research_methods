#!/bin/bash

# Get all the twitter data from the files that contain the whole string "hedde".
# Zless will get the files content. ruby-F'"text":' etc .. will output the text posted by the user, grep -w -i will search for the word 'hedde' and 'Hedde', the -i makes sure both capital and non-capital are shown.
# ruby -F'"location":' etc.. will give all the text after the string '"location":', awk-F makes sure that the location is only cut out, otherwise we will get all the information that comes after the string '"location":'.
# the data used is from the month february in the year 2019

zless *.out.gz | ruby -F'"text":' -ane  'puts $F' | grep -w -i 'hedde' | ruby -F'"location":' -ane  'puts $F' | awk -F',"' '{print $1}'




